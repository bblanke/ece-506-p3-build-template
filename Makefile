all: build

build:
	docker build -t ece506p3 .

test:
	docker run -it --mount type=bind,source="$(shell pwd)/trace",target=/root/code/trace ece506p3

clean:
	cd code && $(MAKE) clean
