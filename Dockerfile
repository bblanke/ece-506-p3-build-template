FROM ubuntu:latest
MAINTAINER Bailey Blankenship (bblanke@ncsu.edu)

RUN apt-get update && apt-get install -y g++ make

COPY ./code /root/code
WORKDIR /root/code
RUN make

CMD ["make", "test"]
