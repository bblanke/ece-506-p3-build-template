# ECE 506 Project 3 Build Template

## Motivation
For project 3, we are provided a compiled version of the "working" program to use while validating our program. Unfortunately, we are only provided a binary that works on Linux. I have a Mac, so my options are:
* SSH into NCSU's servers and perform all of my text editing in VIM/Nano
* Run a Virtualbox Linux image, SSH into it, and do my development in VIM/nano
* Run a Virtualbox Linux image and bang my head against a wall until the GUI responds
* Something else I haven't thought of
* Develop on my platform of choice (Mac) in my text editor of choice (Atom) and
	just perform all of my building/testing in a Docker image.

We're gonna go with the last one. Docker is a tool to programatically launch and manage lightweight virtual machines. With Docker, we can just add a few commands into our Makefile that automatically create a Linux VM, build/test the code in it, and return the results.

## Instructions
1. Install [Docker Desktop](https://www.docker.com/products/docker-desktop). I'm running version 2.2.0.3. A newer version is probably OK.
2. Clone this repo to your machine. Fire up your terminal and `cd` into this project.
3. Download the project code from the course website, and copy/paste the *.cc and *.h files from the project's `code` folder into the `code` folder of this project **Don't replace the Makefile, module.sh, or simulate_cache_ref**. Keep the files provided by this project. They're basically the same as those provided by the project, but to prevent hiccups, just use the ones I've provided.
4. Download the trace file mentioned in the project specification and place it in the root directory of this project (where the README, Dockerfile, etc. are stored). **Make sure that it's named trace (no file extension)**
5. To build your code, run `make build`
6. To test your code, run `make test`
7. To change the parameters for testing (protocol, associativity, etc.), edit the `test` command in `code/Makefile`
8. Post any questions to the Piazza thread.



#### Review

Your project directory should look like this:

```
ece506p3/
  .git
  code/
    cache.cc // you'll put these here
    cache.h
    ...more cc/h files
    Makefile // edit this to change test params
    module.sh // Edit this to change the output of test results
    simulate_cache_ref // you'll add 
    ...more cc/h files
  .dockerignore
  .gitignore
  Dockerfile
  Makefile // you shouldn't need to edit this much
  README.md
  trace // you'll download the trace file from the project spec and put it here
```



## How this works

```mermaid
graph TD
	a[Run 'make build' on your terminal]
	b[Docker starts a new linux vm and copies the 'code' directory into it]
	a-->b
	c["Docker calls 'make' from the Makefile in the 'code' directory to build the code. This happens within the VM"]
	b-->c
	d[Run 'make test' on your terminal]
	c-->d
	e[Docker mounts the 'trace' file to the VM so that it can access it for testing]
	f[Docker runs 'make test', which calls 'make test' in the 'code' directory's Makefile. This happens in the VM]
	d-->e
	g[Results are spat out of the VM into your terminal]
	e-->f
	f-->g
```





## Original README from project files

To run the program, issue command:
./simulate_cache_ref smp 131072 1 32 4 10 trace_file_path > output_name.txt

L1_SIZE:					131072
L1_ASSOC:					1
L1_BLOCKSIZE:				32
NUMBER OF PROCESSORS:		4
COHERENCE PROTOCOL:			0 : MSI
							4 : Firefly
							5 : MESI
							10: Dragon

For canneal, the number of processors need to be 4.
For fluid, the number of processors need to be 16.
Wrong specification will give you segmentation fault.
